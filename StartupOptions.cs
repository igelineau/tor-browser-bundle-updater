﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TorBrowserUpdater
{
    static class StartupOptions
    {
        public static string Language { get; set; }
        public static bool Force32Bit { get; set; }

        public static string Mirror { get; set; }

        public static bool SkipSignatureVerification { get; set; }
        public static bool KeepSignatureFile { get; set; }
        public static string PublicKeyFile { get; set; }

        public static bool Verbose { get; set; }

        public static bool UsePluggableTransports { get; set; }
    }
}
