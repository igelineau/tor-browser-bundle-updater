﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TorBrowserUpdater
{
    class TBBUpdaterException : Exception
    {

        public TBBUpdaterException(string message)
            : base(message)
        { }

        public TBBUpdaterException(string message, params object[] formatArgs)
            : base(string.Format(message, formatArgs))
        { }

        public TBBUpdaterException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
