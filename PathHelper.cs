using System;
using System.IO;

namespace TorBrowserUpdater
{
	public class PathHelper
	{
		
		public static string TorStarterRelPath
		{
			get
			{
				switch(Environment.OSVersion.Platform)
				{
				case PlatformID.Unix:
					return "start-tor-browser";
				default:
					return "Start Tor Browser.exe";
				}
			}
		}
		
		public static string TorExeRelPath
		{
			get
			{
				switch(Environment.OSVersion.Platform)
				{
				case PlatformID.Unix:
					return "App/tor";
				default:
					return "App/tor.exe";
				}
			}
		}
		
		public static string VidaliaExeRelPath
		{
			get
			{
				switch(Environment.OSVersion.Platform)
				{
				case PlatformID.Unix:
					return "App/vidalia";
				default:
					return "App/vidalia.exe";
				}
			}
		}
		
		public static string FirefoxRelDir
		{
			get
			{
				switch(Environment.OSVersion.Platform)
				{
				case PlatformID.Unix:
					return "App/Firefox";
				default:
					return "FirefoxPortable";
				}
			}
		}
		
		public static string FirefoxExeRelDir
		{
			get
			{
				switch(Environment.OSVersion.Platform)
				{
				case PlatformID.Unix:
					return Path.Combine(FirefoxRelDir, "firefox");
				default:
					return Path.Combine(FirefoxRelDir, "FirefoxPortable", "firefox.exe");
				}
			}
		}
		
		public static string FirefoxProfileRelPath
		{
			get
			{
				switch(Environment.OSVersion.Platform)
				{
				case PlatformID.Unix:
					return Path.Combine("Data", "profile");
				default:
					return Path.Combine(FirefoxRelDir, "Data","profile");
				}
			}
		}

	    /// <summary>
	    /// Returns the relative path of the root folder of TBB, accordingly to the platform we're running on.
	    /// </summary>
	    public static string GetUncompressedTBBFolderPath(string languageString)
	    {
	        switch (Environment.OSVersion.Platform)
	        {
	            case PlatformID.Unix:
	                return Path.Combine(Environment.CurrentDirectory, "tor-browser_" + languageString);
	            default:
	                return Path.Combine(Environment.CurrentDirectory, "Tor Browser");
	        }
	    }
	}
}

