﻿
namespace TorBrowserUpdater
{
    public static class SevenZipHelper
    {
        const string SevenZipFilename = "7za.exe";

        public static void ExtractArchiveToCurrentDirectory(string archiveName)
        {
            string arguments = "x -y " + archiveName;
            Tools.StartProgram(SevenZipFilename, arguments);
        }
    }
}
