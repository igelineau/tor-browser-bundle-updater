﻿Requirements for the executable

1- Requirements for any OS

-HtmlAgilityPack.dll (available here : http://htmlagilitypack.codeplex.com/)
-BouncyCastle.Crypto.dll (available here : http://www.bouncycastle.org/csharp/, choose Compiled assembly only)

2- Windows only

-7-Zip portable version (available here : http://portableapps.com/apps/utilities/7-zip_portable)

3- Linux only

-There is currently no dependencies for Linux, excepted those in the 1st section