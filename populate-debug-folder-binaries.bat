PUSHD %~DP0

SET destFolder=bin\Debug\

XCOPY "packages\7-Zip.CommandLine.9.20.0\tools\7za.exe" %destFolder% /Y

XCOPY "packages\WGETWindows.1.11.4\wget.exe" %destFolder% /Y

XCOPY "packages\HtmlAgilityPack.1.4.6\lib\Net40\HtmlAgilityPack.dll" %destFolder% /Y
XCOPY "packages\HtmlAgilityPack.1.4.6\lib\Net40\HtmlAgilityPack.pdb" %destFolder% /Y
XCOPY "packages\HtmlAgilityPack.1.4.6\lib\Net40\HtmlAgilityPack.xml" %destFolder% /Y

XCOPY "packages\BouncyCastle.1.7.0\lib\Net40-Client\BouncyCastle.Crypto.dll" %destFolder% /Y
XCOPY "packages\BouncyCastle.1.7.0\lib\Net40-Client\BouncyCastle.Crypto.pdb" %destFolder% /Y

POPD