PUSHD %~DP0

SET version=1.0.1
SET exeFile=TorBrowserUpdater.exe
SET pdbFile=TorBrowserUpdater.pdb

MKDIR release

SET winFolder=release\tor-browser-bundle-updater-win32-%version%
MKDIR %winFolder%
XCOPY "bin\Release\%exeFile%" %winFolder% /Y
XCOPY "bin\Release\%pdbFile%" %winFolder% /Y
XCOPY "packages\7-Zip.CommandLine.9.20.0\tools\7za.exe" %winFolder% /Y
XCOPY "packages\WGETWindows.1.11.4\wget.exe" %winFolder% /Y
XCOPY "packages\HtmlAgilityPack.1.4.6\lib\Net40\HtmlAgilityPack.dll" %winFolder% /Y
XCOPY "packages\BouncyCastle.1.7.0\lib\Net40-Client\BouncyCastle.Crypto.dll" %winFolder% /Y

SET linuxFolder=release\tor-browser-bundle-updater-linux-%version%
MKDIR %linuxFolder%
XCOPY "bin\Release\%exeFile%" %linuxFolder% /Y
XCOPY "bin\Release\%pdbFile%" %linuxFolder% /Y
XCOPY "packages\HtmlAgilityPack.1.4.6\lib\Net40\HtmlAgilityPack.dll" %linuxFolder% /Y
XCOPY "packages\BouncyCastle.1.7.0\lib\Net40-Client\BouncyCastle.Crypto.dll" %linuxFolder% /Y

POPD