﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TorBrowserUpdater
{
    class TorBrowserBundleRetrieverTest
    {

        public static void StartTests()
        {
            try
            {
                StartupOptions.UsePluggableTransports = true;
                TestDownload("en-US", true, TBBUpdater.PlatformType.Windows);
                TestDownload("en-US", false, TBBUpdater.PlatformType.Windows);
                TestDownload("en-US", true, TBBUpdater.PlatformType.Linux);
                TestDownload("en-US", false, TBBUpdater.PlatformType.Linux);

                StartupOptions.UsePluggableTransports = false;
                TestDownload("en-US", true, TBBUpdater.PlatformType.Windows);
                TestDownload("en-US", false, TBBUpdater.PlatformType.Windows);
                TestDownload("en-US", true, TBBUpdater.PlatformType.Linux);
                TestDownload("en-US", false, TBBUpdater.PlatformType.Linux);

                TestDownload("fr", true, TBBUpdater.PlatformType.Windows);
                TestDownload("fr", false, TBBUpdater.PlatformType.Windows);
                TestDownload("fr", true, TBBUpdater.PlatformType.Linux);
                TestDownload("fr", false, TBBUpdater.PlatformType.Linux);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Print(e.ToString());
            }
        }

        private static void TestDownload(string language, bool force32Bit, TBBUpdater.PlatformType platform)
        {
            if(Directory.Exists("Tor Browser"))
                Directory.Delete("Tor Browser", true);

            var updater = new TBBUpdater(language, force32Bit, platform);
            updater.DownloadReleaseIfNewerExists();
            if(TBBUpdater.ErrorOccurred)
            {
                System.Diagnostics.Debug.WriteLine(TBBUpdater.Error.ToString());
                Console.Read();
            }
        }

    }
}
